<?php

namespace App\Controller;

use App\Entity\Bathroom;
use App\Form\BathroomType;
use App\Repository\BathroomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/bathroom')]
class BathroomController extends AbstractController
{
    #[Route('/', name: 'app_bathroom_index', methods: ['GET'])]
    public function index(BathroomRepository $bathroomRepository): Response
    {
        return $this->render('bathroom/index.html.twig', [
            'bathrooms' => $bathroomRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_bathroom_new', methods: ['GET', 'POST'])]
    public function new(Request $request, BathroomRepository $bathroomRepository): Response
    {
        $bathroom = new Bathroom();
        $form = $this->createForm(BathroomType::class, $bathroom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bathroomRepository->add($bathroom);
            return $this->redirectToRoute('app_bathroom_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bathroom/new.html.twig', [
            'bathroom' => $bathroom,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_bathroom_show', methods: ['GET'])]
    public function show(Bathroom $bathroom): Response
    {
        return $this->render('bathroom/show.html.twig', [
            'bathroom' => $bathroom,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_bathroom_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Bathroom $bathroom, BathroomRepository $bathroomRepository): Response
    {
        $form = $this->createForm(BathroomType::class, $bathroom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bathroomRepository->add($bathroom);
            return $this->redirectToRoute('app_bathroom_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bathroom/edit.html.twig', [
            'bathroom' => $bathroom,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_bathroom_delete', methods: ['POST'])]
    public function delete(Request $request, Bathroom $bathroom, BathroomRepository $bathroomRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bathroom->getId(), $request->request->get('_token'))) {
            $bathroomRepository->remove($bathroom);
        }

        return $this->redirectToRoute('app_bathroom_index', [], Response::HTTP_SEE_OTHER);
    }
}
