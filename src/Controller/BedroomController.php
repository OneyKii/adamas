<?php

namespace App\Controller;

use App\Entity\Bedroom;
use App\Form\BedroomType;
use App\Repository\BedroomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/bedroom')]
class BedroomController extends AbstractController
{
    #[Route('/', name: 'app_bedroom_index', methods: ['GET'])]
    public function index(BedroomRepository $bedroomRepository): Response
    {
        return $this->render('bedroom/index.html.twig', [
            'bedrooms' => $bedroomRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_bedroom_new', methods: ['GET', 'POST'])]
    public function new(Request $request, BedroomRepository $bedroomRepository): Response
    {
        $bedroom = new Bedroom();
        $form = $this->createForm(BedroomType::class, $bedroom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bedroomRepository->add($bedroom);
            return $this->redirectToRoute('app_bedroom_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bedroom/new.html.twig', [
            'bedroom' => $bedroom,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_bedroom_show', methods: ['GET'])]
    public function show(Bedroom $bedroom): Response
    {
        return $this->render('bedroom/show.html.twig', [
            'bedroom' => $bedroom,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_bedroom_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Bedroom $bedroom, BedroomRepository $bedroomRepository): Response
    {
        $form = $this->createForm(BedroomType::class, $bedroom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $bedroomRepository->add($bedroom);
            return $this->redirectToRoute('app_bedroom_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('bedroom/edit.html.twig', [
            'bedroom' => $bedroom,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_bedroom_delete', methods: ['POST'])]
    public function delete(Request $request, Bedroom $bedroom, BedroomRepository $bedroomRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bedroom->getId(), $request->request->get('_token'))) {
            $bedroomRepository->remove($bedroom);
        }

        return $this->redirectToRoute('app_bedroom_index', [], Response::HTTP_SEE_OTHER);
    }
}
