<?php

namespace App\Controller;

use App\Entity\LivingRoom;
use App\Form\LivingRoomType;
use App\Repository\LivingRoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/living-room')]
class LivingRoomController extends AbstractController
{
    #[Route('/', name: 'app_living_room_index', methods: ['GET'])]
    public function index(LivingRoomRepository $livingRoomRepository): Response
    {
        return $this->render('living_room/index.html.twig', [
            'living_rooms' => $livingRoomRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_living_room_new', methods: ['GET', 'POST'])]
    public function new(Request $request, LivingRoomRepository $livingRoomRepository): Response
    {
        $livingRoom = new LivingRoom();
        $form = $this->createForm(LivingRoomType::class, $livingRoom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $livingRoomRepository->add($livingRoom);
            return $this->redirectToRoute('app_living_room_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('living_room/new.html.twig', [
            'living_room' => $livingRoom,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_living_room_show', methods: ['GET'])]
    public function show(LivingRoom $livingRoom): Response
    {
        return $this->render('living_room/show.html.twig', [
            'living_room' => $livingRoom,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_living_room_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, LivingRoom $livingRoom, LivingRoomRepository $livingRoomRepository): Response
    {
        $form = $this->createForm(LivingRoomType::class, $livingRoom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $livingRoomRepository->add($livingRoom);
            return $this->redirectToRoute('app_living_room_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('living_room/edit.html.twig', [
            'living_room' => $livingRoom,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_living_room_delete', methods: ['POST'])]
    public function delete(Request $request, LivingRoom $livingRoom, LivingRoomRepository $livingRoomRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$livingRoom->getId(), $request->request->get('_token'))) {
            $livingRoomRepository->remove($livingRoom);
        }

        return $this->redirectToRoute('app_living_room_index', [], Response::HTTP_SEE_OTHER);
    }
}
